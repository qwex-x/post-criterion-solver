//-------------helpers-----------------
function toBin(number, l) {
  const result = [];
  for (let i = 0; i < l; ++i) {
    result.push(number & 1); 
    number = number >> 1;
  }
  return result.reverse();
}

function fromBin(array)
{
  if (typeof array === 'string')
    array = array.split('').map(x => Number(x))

  let sum = 0
  array = array
  for (let i = 0; i < array.length; ++i)
    sum = (sum << 1) + array[i]
  return sum
}

function compare(left, right, shift) {
  if (Array.isArray(left))
    left  = fromBin(left)
  if (Array.isArray(right))
    right = fromBin(right) 

  const mask = (1 << shift) - 1

  return ((
    (left ^ mask) | right
  ) ^ mask) === 0 
}

function negate(left, shift) {
  return toBin(fromBin(left) ^ ((1 << shift) - 1 ), shift)
}

//-------------checkers------------------------------

function T1(number) {
  if (Array.isArray(number))
    number = fromBin(number)

  return (number & 1) === 1;
}

function T0(number) {
  if (Array.isArray(number))
    number = fromBin(number)


  return ((number >> 7) & 1) === 0;
}

function L(number) {
  function helper(number, position) {
    let next = number ^ (((number << 1) & 255) | (number & 1))
    if (position <= 7) {
      if ((toBin(number, 8)[0] === 1) && ![0,1,2,4].includes(position))
        return false
      else
        return helper(next, position + 1)
    } else return true;
  }

  if (Array.isArray(number))
    number = fromBin(number)
  return helper(number, 0);
}

function S(number) {
  if (Array.isArray(number))
    number = fromBin(number)

  let left  = fromBin(toBin((number & 240) >> 4, 4).reverse())
  let right = number & 15
 
  return (left ^ right) === 15 
}

function M(number) {
  if (Array.isArray(number))
    number = fromBin(number)

  function helper(number, shift) {
    if (shift == 0)
      return true;

    const mask = (1 << shift) - 1

    let left   = (number & (mask << shift) ) >> shift
    let right  = number & mask

    if (!compare(left, right, shift)) {
      return false
    }
    else return (
      helper(left, shift >> 1) && helper(right, shift >> 1)
    )
  }

  
  return helper(number, 4)
}

// (~x + y) ^ 1 = ( (x ^ 1) + y) ^ 1 = 

// x y z | x + y | a + z | x ^ z | y ^ z | f

// 0 0 0 |   0   |   0   |   0   |   0   | 0 
// 0 0 1 |   0   |   1   |   1   |   1   | 0
// 0 1 0 |   1   |   1   |   0   |   1   | 1
// 0 1 1 |   1   |   0   |   1   |   0   | 1
// 1 0 0 |   1   |   1   |   1   |   0   | 1
// 1 0 1 |   1   |   0   |   0   |   1   | 1
// 1 1 0 |   1   |   1   |   1   |   1   | 0
// 1 1 1 |   1   |   0   |   0   |   0   | 0